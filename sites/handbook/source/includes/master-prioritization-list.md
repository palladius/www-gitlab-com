| Priority | Description | Issue label(s) |
| ------ | ------ | ------ |
| 1* | <a href="/handbook/engineering/security/#severity-and-priority-labels-on-security-issues">Security</a> | `bug::vulnerability` |
| 2* | Data Loss | `data loss` |
| 3* | Resilience, Reliability, <a href="/handbook/engineering/performance/index.html#availability">Availability</a>, <a href="/handbook/engineering/workflow/#infradev">and Performance</a>| `availability`, `infradev`, `Corrective Action`, `bug::performance` |
| 4 | Usability | `Usability benchmark`, `SUS::Impacting`, `UX debt` |
| 5 | Instrumentation | `instrumentation` |
| 6 | xMAU / ARR Drivers | `direction` |
| 7 | All other items not covered above | |

*indicates forced prioritization items with SLAs/SLOs
